# Notes
Summaries of the papers. This file is very raw. Either eng or slo is ok. 
H2 should be the paper name.

## Terms
name | description
--- | ---
free word order | It's more of a spectrum... Languages with free word order have more flexible word order. They compensate with rich morphological markings.  
DG | Dependency grammar is a syntactic theory, opposite of constituency grammar or phrase-structure grammar. It's based on dependencies - directional linkc between the head (usually verb) and its dependants. The theory's useful for languages with free word order (Czech, Slowak, Turkish, ...).
FGD | Functional generative description is a stratificational grammar formalism that treats the sentence as a system of interlinked layers: phonological, morphematical, morphonological, analytical (surface syntax) and tectogrammatical (deep syntax). See also [layers in PDT](https://ufal.mff.cuni.cz/pdt2.0/doc/pdt-guide/en/html/ch02.html).  
functor | [doc](https://ufal.mff.cuni.cz/pdt2.0/doc/manuals/en/t-layer/html/ch07.html) A functor represents the semantic value of a sintactic depencency relation. Here are two prominent ways we can classify functors. Part-of speech division: there are functors assigned to nodes dependent exclusively on nouns/verbs; valency: attribute/adjunct.  
lexeme | Unit of lexical meaning that exists regardless of the number of inflection endings it has or words it contins. It's an abstract unit of morphological analysis in linguistics that roughly corresponds to a set of forms taken by a single word.  
XML-TEI | xml formata TEI (Text Encoding Initiative). TEI je standard za oznacevanje textov v obliki, ki je berljiva za stroje.  
SLR | Semantic Role Labeling is an abstraction lower than syntax-tree (it has more classes). It means finding the predicate (verb) of the sentence and assigning roles to other parts of the sentence in respect to the verb.  (Functors? Is this my thing?)
MSD | MorphoSyntactic Description - oblikoskladenjska oznaka
morpheme | Smallest lexical unit, smaller than word (word is always freestanding). *Cat* is a root morpheme, also a word. *Cats* consists of root morpheme *cat* and an affix *-s*. *Cats* is also a word, while *-s* is a morpheme but no a word.  

## Leading question: 
How did they make crovallex?  

## preradovic2009crovallex.pdf
Flecting language?

Surface form? - morphemosyntactic form?  

Pristop grajenja CROVALLEX je oboje sintakticen in semanticen.  
Uporaba sintakse za dolocanje ACT in PAT (1. and 2. verb complement). [8]  
Uporaba semantike za dolocanje ostalih "inner participants (arg) and free modifications (adjuncts)". [1]  

Sintakticno-semanticni razredi so dodeljeni glagolom v leksikonu. Izpeljani so iz Levinovih razredov. Also check VerbNet. [10]  

## preradovic2010semantic.pdf

## preradovic2013verbal.pdf

## duvsek2014verbal.pdf
Edmonds and Cotton, 2001; Chen and Palmer, 2005 -- these two are useless
Automatic valency frame detection using logistic regression. Might be what I need. 

### Abstract
We present a supervised learning method
for verbal valency frame detection and se-
lection, i.e., a specific kind of word sense
disambiguation for verbs based on subcat-
egorization information, which amounts
to detecting mentions of events in text.
We use the rich dependency annotation
present in the Prague Dependency Tree-
banks for Czech and English, taking ad-
vantage of several analysis tools (taggers,
parsers) developed on these datasets pre-
viously. The frame selection is based on
manually created lexicons accompanying
these treebanks, namely on PDT-Vallex for
Czech and EngVallex for English. The re-
sults show that verbal predicate detection
is easier for Czech, but in the subsequent
frame selection task, better results have
been achieved for English.

### Notes
FGD (Functional Generative Description): valenca je osnova za gradnjo t-layerja (tectogrammatical layer). 
PDT (Prague dependency treebank) zgrajen na osnofi FGD.  
4 layers: [pdt-layers](#user-content-pdt-layers)

V clanku: valenca glagolov se ugotavlja z uporabo 2 dimenzij:

* inner participants (arguments) - free modifications (adjuncts).
VALENCZ FRAME = arguments(optional+obligatory) + adjuncts(obligatory)
* razlika med semanticno obveznimi in neobveznimi komponentami

Klasifikacija z uporabo vseh 4 layerjev v PDT (?).  
classification algorithm: logistic regression


## PDT 2.0 Documentation

### PDT layers <a name="pdt-layes"></a>

* **w-layer, word layer**  
Counts more like a preprocessing layer, consists of tokens.  
* **m-layer, morphological layer**  
Contains lemmas from the word layer tokens, also contains morphological information for each lemma. Linear.  
* **a-layer, analytical layer** (surface dependency syntax tree)  
Conains a shallow syntactic tree with 1-1 links to m-layer nodes. Syntactic dependency information. Linear.  
* **t-layer, tectogrammatical layer** (deep dependency syntax tree, based on verb valency)  
Contains deep syntactic tree. Nodes are assigned a number of attributes including functors. Also links to all former layer nodes. Every verb or certain-type noun links to his valency frame. Functors point from nodes to root (head?).  


### Verb valency
Valency modifications are in a broader sense all kinds of elements that can affect the meaning of a lexical unit. In stricter sense (our sense), they are inner participants and obligatory free modifiers.  

**Inner participants** or **arguments** are such modifiers that can modify a verb only once and can modify only a certain class of verbs.  
**Free modifiers** or **adjuncts** are such modifiers that can modify a verb several times and apply to all verbs in general.  

\\ | how many verbs it modifies | how many times can be applied
--- | ---| ---
**argument** | certain class of verbs | once
**adjunct** | all verbs | multiple times

Main arguments: ACT, PAR, ADDR, ORIG, EFF.
There are many more adjuncts: [List of PDT functors](https://ufal.mff.cuni.cz/pdt2.0/doc/manuals/en/t-layer/html/ch07.html).
For more infor on Annotation on the tectogrammatical layer: [link](https://ufal.mff.cuni.cz/pdt2.0/doc/manuals/en/t-layer/html/index.html).  

#### Obligatory vs optional arguments/adjuncts
"The dialogue test." - After a sentence, if we ask after an argument, in some cases the talker must know the answer. 
Example:
"He bought it."
"Who bought it?" (ACT), correct, obl.
"For whom did he buy it?" not necessary, opt. - still in the valency frame though.

"My friends came." ("here" implied)
"Where did they come?" correct, obl.
"Where did they come fom?" not necessary, opt.

#### Determining arguments
If one: ACT.
If two: ACT, PAR. Ususlly this sequence, some exceptions.  
If nominatove + dative and the dative one is the experiencer, the experiencer becomes ACT, the first one is PAR.  
Example: "Kniha se mi libila" ("I liked the book.") - Kniha becomes PAR.  


## cinkova2006propbank

### my_Abstract
Semi-automatically building EngVallex from PropBank. Propbank consists of **rolesets**. One rolset ~ one meaning of the verb.  
Roleset includes a set of labels (arg0, arg1, arg2, ...). It's built using altered Levinan verb classes. 
EngVallex is a FGD-compliant lexicon. rolesets are transformed into verb vrames, labels (arg0, ...) are mapped onto functors (ACT, PAR, ...).  
Automatic mapping of higher-lvl args to adjuncts about 90% correct. Low lvl args had to be manually checked.  

In the paper also a summary of Vallex structure and frame structure.  

### Interesting notes on automatic mapping onto EngVallex

* In frames with intransitive sentences and transitive sentences, ACT and PAT have been systematically exchanged.  
Example: "The choir(ACT) is singing carols(PAT)." - "Carols(ACT) are being sung by the choir."  
* Merging frames with the same surface syntax structure.  


## lopatkova2007towards

Vsebuje smernice za oznacevanje, veliko primerov. 


## dobrovoljc2012skladenjski

Dokumentacija o skladenjskem razčlenjevalniku. 
Obstaja več vrst jezikoslovnih modelov:

* teoretični
    * sistem odvisnostnih drevesnic (jeziki s prostim besednim redom. slo)
    * frazna gramatika (ang)
* strojne metode
    * razčlenjevanje na podlagi vnaprej pripravljenih pravil
    * **statistično razčlenjevanje** (potrebujemo učni korpus. ssj500k?)
    * hibridni postopki

**MSTParser** je bila podlaga za razvoj razčlenjevalnika. Teorija [minimum spanning trees](https://en.wikipedia.org/wiki/Minimum_spanning_tree). 

### JOS - jezikovno ozna£evanje sloven$£ine. 
Projekt JOS je razvil dva ozna£ena korpusa slovenskega jezika: jos100K in jos1M.  
Razvil je tudi [oblikoskladenjske specifikacije JOS](http://nl.ijs.si/jos/msd/html-sl/index.html).  
Seznam vseh MSD lahko med drugim najdemo zraven korpusa jos100K.  
Primer: Ggdn pomeni G - glagol, g - glavni, d - dovrsni, n - oblika je nedolocnik.  

### ssj500K
Korpus sestavljen iz *jos100K* ter 400.000 besed iz korpusa *jos1M*.  
Del, ki je izpeljan iz jos100K ima dodane informacije o lastnih imenih za potrebe strojnega prepoznavanja imenskih entitet.  

* <link> skladenjske povezave  
* <chunk> imenske entitete?


## Ponovitev gramatike
(ni vezano na članek)

* besedne zveze
    * podredne
        "Lepa Vida."
    * priredne
        "Oče in mati."
    * soredne.
* stavek
    je jezikovna enota iz več besed, zbranih okoli osebne glagolske oblike oz. povedka.  
* stavčni členi
    * osebek
    * povedek (predikat)
        je stavčni člen, ki pove "Kaj kdo dela ali kaj se z njim dogaja."
        Ponavadi sestoji iz osebne glagolske oblike. 
        Ostali stavčni členi se vežejo na povedek.  
    * predmet
    * prislovno določilo
* povedkovo določilo
    stoji ob pomožnem glagolu (vezi).
    "je še mlad" - cela stvar je povedek, *mlad* je povedkovo določilo.
* osebna glagolska oblika
    je oblika glagola, ki izraža osebo.  
    "Pomivam posodo." protiprimer: "vstaviti CD"
* predlog
    povezuje dve neenakovredni besedi v krajevnem, časovmen, načinovnem in vzročnem razmerju.  
    "v, med, iz, zaradi"
* prilastek (atribut)
    je stavčni člen, ki ima v povedi običajno obliko določila.
    "prelepo jutro", "ptica pevka", "torta iz jagod"

* dopolnilo
    glej [vezljivost](https://www.leemeta.si/blog/slovnica/valenca-in-glagoli)

* osebkov odvisnik  
    Osebkov odvisnik dopolnjuje povedek glavnega stavka s podatkom o vršilcu dejanja. Po njem se lahko vprašamo z vprašalnico kdo ali kaj + povedek glavnega stavka. Glavni stavek je od odvisnega vedno ločen z vejico.  
    "Kdor ne dela naj ne je."

* predmetni odvisnik 
    "Pogovorimo se o tem, kaj lahko naredimo."  

* prislovni odvisnik
    ", ki...""

! Pazi na ločevanje med Besednimi zvezami in besednimi vrstami.  


## DATA
(ni vezano na clanek)

* ssj200k  
Dobil od Simona kreka. Vsebuje functorje. (Ucni korpus za valenco?)
```xml
<s xml:id="S13" n="?">
    <w xml:id="S13.t1" msd="Vp" lemma="zato">Zato</w>
    <w xml:id="S13.t2" msd="Gp-ste-n" lemma="biti">je</w>
    <w xml:id="S13.t3" msd="Rsn" lemma="včeraj">včeraj</w>
    <w xml:id="S13.t4" msd="Ggdd-ez" lemma="sklicati">sklicala</w>
    <w xml:id="S13.t5" msd="Sometn" lemma="sestanek">sestanek</w>
    <c xml:id="S13.t6">,</c>
    <w xml:id="S13.t7" msd="Dm" lemma="na">na</w>
    <w xml:id="S13.t8" msd="Zv-mem" lemma="kateri">katerem</w>
    <w xml:id="S13.t9" msd="Gp-stm-n" lemma="biti">so</w>
    <w xml:id="S13.t10" msd="Ggnd-mm" lemma="ugotavljati">ugotavljali</w>
    <c xml:id="S13.t11">,</c>
    <w xml:id="S13.t12" msd="Rsn" lemma="kako">kako</w>
    <w xml:id="S13.t13" msd="Gp-g" lemma="biti">bi</w>
    <w xml:id="S13.t14" msd="Ggdd-mm" lemma="priti">prišli</w>
    <w xml:id="S13.t15" msd="Dr" lemma="do">do</w>
    <w xml:id="S13.t16" msd="Ppszer" lemma="primeren">najprimernejše</w>
    <w xml:id="S13.t17" msd="Sozer" lemma="rešitev">rešitve</w>
    <c xml:id="S13.t18">.</c>
    <c xml:id="S13.t"></c>
    <linkGrp type="dep">
      <link from="S13.t4" afun="vez" dep="S13.t1" />
      <link from="S13.t4" afun="del" dep="S13.t2" />
      <link from="S13.t4" afun="štiri" dep="S13.t3" />
      <link from="S13.t0" afun="modra" dep="S13.t4" />
      <link from="S13.t4" afun="dve" dep="S13.t5" />
      <link from="S13.t0" afun="modra" dep="S13.t6" />
      <link from="S13.t8" afun="dol" dep="S13.t7" />
      <link from="S13.t10" afun="vez" dep="S13.t8" />
      <link from="S13.t10" afun="del" dep="S13.t9" />
      <link from="S13.t5" afun="dol" dep="S13.t10" />
      <link from="S13.t0" afun="modra" dep="S13.t11" />
      <link from="S13.t14" afun="vez" dep="S13.t12" />
      <link from="S13.t14" afun="del" dep="S13.t13" />
      <link from="S13.t10" afun="dve" dep="S13.t14" />
      <link from="S13.t17" afun="dol" dep="S13.t15" />
      <link from="S13.t17" afun="dol" dep="S13.t16" />
      <link from="S13.t14" afun="dve" dep="S13.t17" />
      <link from="S13.t0" afun="modra" dep="S13.t18" />
    </linkGrp>
    <linkGrp type="SRL">
      <link from="S13.t4" afun="TIME" dep="S13.t3" />
      <link from="S13.t4" afun="PAT" dep="S13.t5" />
      <link from="S13.t10" afun="PAT" dep="S13.t14" />
      <link from="S13.t14" afun="RESLT" dep="S13.t17" />
    </linkGrp>
</s>
```
* ssj500k
Ucni korpus za razclenjevalnik. Primer povedi:  
```xml
<s xml:id="ssj1219.5967.20747">
  <w xml:id="ssj1219.5967.20747.t1" ana="msd:Ppnzei" lemma="dobrodošel">Dobrodošla</w>
  <c> </c>
  <w xml:id="ssj1219.5967.20747.t2" ana="msd:Gp-ste-n" lemma="biti">je</w>
  <c> </c>
  <w xml:id="ssj1219.5967.20747.t3" ana="msd:L" lemma="tudi">tudi</w>
  <c> </c>
  <w xml:id="ssj1219.5967.20747.t4" ana="msd:Ppnzei" lemma="nov">nova</w>
  <c> </c>
  <w xml:id="ssj1219.5967.20747.t5" ana="msd:Sozei" lemma="možnost">možnost</w>
  <c> </c>
  <w xml:id="ssj1219.5967.20747.t6" ana="msd:Ppnzmr" lemma="elastičen">elastičnih</w>
  <c> </c>
  <w xml:id="ssj1219.5967.20747.t7" ana="msd:Sozmr" lemma="mapa">map</w>
  <c> </c>
  <pc xml:id="ssj1219.5967.20747.t8" ana="msd:U">(</pc>
  <w xml:id="ssj1219.5967.20747.t9" ana="msd:Slmei" lemma="Spring">Spring</w>
  <c> </c>
  <w xml:id="ssj1219.5967.20747.t10" ana="msd:Slmei" lemma="Folders">Folders</w>
  <pc xml:id="ssj1219.5967.20747.t11" ana="msd:U">)</pc>
  <pc xml:id="ssj1219.5967.20747.t12" ana="msd:U">.</pc>
  <c> </c>
</s>
```
Ta primer je nekoliko osnoven. Korpus naj bi vseboval tudi *nekatere* povedi z dodanimi skladenjskimi informacijami in vec... Isci <links> in <chunks>. Korpus je bil tudi rocno pregledan.  
* jos100k
Rocno oznacen korpus. Info/publikacije [tule](http://nl.ijs.si/jos/jos100k-sl.html).
```xml
<s xml:id="F0015653.211.5" n="10 11">
    <w xml:id="F0015653.211.5.1" lemma="ob" msd="Dm">Ob</w>
    <S/>
    <w xml:id="F0015653.211.5.2" lemma="hrvaški" msd="Ppnzem">hrvaški</w>
    <S/>
    <w xml:id="F0015653.211.5.3" lemma="obala" msd="Sozem">obali</w>
    <S/>
    <w xml:id="F0015653.211.5.4" lemma="pravzaprav" msd="L">pravzaprav</w>
    <S/>
    <w xml:id="F0015653.211.5.5" lemma="izvoren" msd="Ppnzmr">izvornih</w>
    <S/>
    <w xml:id="F0015653.211.5.6" lemma="peščen" msd="Ppnzmr">peščenih</w>
    <S/>
    <w xml:id="F0015653.211.5.7" lemma="sipina" msd="Sozmr">sipin</w>
    <S/>
    <w xml:id="F0015653.211.5.8" lemma="niti" msd="Vp">niti</w>
    <S/>
    <w xml:id="F0015653.211.5.9" lemma="biti" msd="Gp-ste-d">ni</w>
    <S/>
    <w xml:id="F0015653.211.5.10" lemma="več" msd="L">več</w>
    <c xml:id="F0015653.211.5.11">.</c>
    <S/>
</s>
<linkGrp type="syntax" targFunc="head argument" corresp="#F0015653.211.5">
    <link type="dol" targets="#F0015653.211.5.3 #F0015653.211.5.1"/>
    <link type="dol" targets="#F0015653.211.5.3 #F0015653.211.5.2"/>
    <link type="stiri" targets="#F0015653.211.5.9 #F0015653.211.5.3"/>
    <link type="modra" targets="#F0015653.211.5 #F0015653.211.5.4"/>
    <link type="dol" targets="#F0015653.211.5.7 #F0015653.211.5.5"/>
    <link type="dol" targets="#F0015653.211.5.7 #F0015653.211.5.6"/>
    <link type="ena" targets="#F0015653.211.5.9 #F0015653.211.5.7"/>
    <link type="modra" targets="#F0015653.211.5 #F0015653.211.5.8"/>
    <link type="modra" targets="#F0015653.211.5 #F0015653.211.5.9"/>
    <link type="modra" targets="#F0015653.211.5 #F0015653.211.5.10"/>
    <link type="modra" targets="#F0015653.211.5 #F0015653.211.5.11"/>
</linkGrp>
```
* VSSG
Andreja Zele?  

### ssj200K
(ni vezano na clanek)  

Zgradba korpusa
```
p                   : 1
s                   : 11408
c                   : 46929
w                   : 200217
linkGrp [dep]       : 11408
link                : 287656
linkGrp [SRL]       : 10687
```

Functors:  
```
ACT
PAT
ORIG

ACMP
AIM
CAUSE
COND
CONTR
DUR
EVENT
FREQ
GOAL
LOC
MANN
MEANS
MODAL
MWPRED
PHRAS
QUANT
REC
REG
RESLT
SOURCE
TIME
------
N:  24
```
Compared to PDT, ADDR and EFF are not present in the main functors group.  

## jurafsky2014speech
This one is AWESOME!!!
book: [Speech and Language processing](https://web.stanford.edu/~jurafsky/slp3/)
The 3rd edition is a draft. Still better than 2nd edition from 2014.  

Overview of what I need for ML SRL. I've already got tokenized with all 4 FGD layers(right?).  
Use the jos200_SRL to train a model. Feature selection in the paper. 

You need to build 2 things: 
Lexicon of frames. Each frame ponints to verbs.
Lexicon of verbs. Each verb points to frames.

TODO finish the second half of this chapter. I'm finally feeling optimistic about this assignment :)

## gildea2002automatic
TODO Details on ML SRL.  


## TODO:

Slovenske zadeve:
* JOS

Bolj vezano na PDT:

* FrameNet, WordNet, VerbNet - some example banks
* Loptakova 2003, (Hajic 2003, Stranalova-Lopatkova, Zabokrtsky 2002) - PDT-Vallex overview

