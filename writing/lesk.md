﻿# Lesk

## Osnovni algoritem
Leskov algoritem je eden klasičnih algoritmov za razdvoumljanje pomena besed. Zasnovan je bil leta 1986, do danes pa je bil deležen številnih revizij in posodobitev.  
Algoritem naj bi v osnovi služil kot cenejša alternativa algoritmom, ki besedila razdvoumljajo s pomočjo slovnične strukture. Leskov algoritem uporablja že obstoječe in računalniško berljive slovarske opise besed v povedih.  
Poved lahko vsebuje večpomenske besede. Vsak pomen določene besede ima v slovarju svoj opis. 
Če med seboj primerjamo opise parov besed, opazimo podobnost med opisi, ki se nanašajo na podoben pomen. Bolj natančno, podobni opisi vsebujejo večje število skupnih besed. S tem lahko najdemo slovarski opis, ki najbolje predstavlja vse besede v povedi.  

Primer:
PINE 
1. kinds of evergreen tree with needle-shaped leaves
2. waste away through sorrow or illness

CONE 
1. solid body which narrows to a point
2. something of this shape whether solid or hollow
3. fruit of certain evergreen trees

Največji presek (po stemmingu*) imata opisa 1. in 3.

Formalno, definirajmo poved P = b0, b1, ... bn, kjer je bi i-ta beseda v povedi.
Slovarsko definicijo j-tega pomena besede bi definirajmo kot def(bi, j).
Presek definicij pomenov (UU*) definirajmo kot število lem*, ki so skupne obema definicijama.
V primeru (cite*) razdvoumljamo dve besedi b1 in b2.
arg max lesk(j, i) = def(b1, j) UU def(b2, i )

Razdvoumljanje celotne povedi zahteva primerjavo parov vseh pomenov besed.

for b in poved:
	for p in pomeni(b):
		for bb in (poved \ b):
			for pp in pomeni(bb):
				p UU pp

Če vzamemo n kot število besed v povedi ter m kot povprečno število pomenov besede, dobimo časovno zahtevnost O(n^2 * m^2).

## Izbojlšave Leskovega algoritma
Razvite so bile številne izboljšave Leskovega algoritma. Glavni spremenljivki pri novejših implementacijah sta izbira glosa*slovaček ter konteksta. 
Glos definiramo kot tekstovne podatke o pomenu besede. Preprosti glos lahko dobimo iz slovarske definicije pomena, medtem ko so kompleksnejši glosi sestavljeni z uporabo leksikalnih verig ter iz učnih množic.
Kontekst so besede, ki obkrožajo tarčno besedo, katere pomen razdvoumljamo. Kot kontekst je lahko izbranih n besed levo in desno od tarčne besede, lahko pa uporabimo cel stavek ali poved, v kateri se tarčna beseda nahaja.


### Simple Lesk
Simple Lesk (SL) je prirejen Leskov algoritem (od zdaj naprej OL), ki uporablja poenostavljen kontekst. Kot kontekst vzame besede okoli tarčne besede in ne njihovih slovarskih definicij. 
Simple Lesk je računsko manj zahteven od originalnega algoritma. Primerjati moramo slovarske pomene tarčne besede z enim kontekstom. Če vzamemo n kot število pomenov tarčne besede, dobimo časovno zahtevnost O(n).

Raziskava je pokazala, da SL daje boljše rezultate od OL. V raziskavi so primerjali oba algoritma, različne velikosti konteksta ter različne glose.
SL se je v vseh poskusih izkazal za boljšega od OL. 
Primerjali so kontekste velikosti 2, 3, 8, 10 in 25 besed. Najprimernejša velikost konteksta se je izkazala za 2 besedi pred in za tarčno besedo. Kontekst velikosti n pomeni n besed pred tarčno besedo in n besed za tarčno besedo. 
Glos so kreirali na štiri načine:
* DEF Slovarska definicija besede,
* REL sopomenke besede ter sopomenke vseh nadpomenk vse do korena slovarja*,
* DEFREL kombinacije DEF in REL,
V primerjavah je bil uporabljen način DEF. Manjši konteksti so dajali boljše rezultate, najboljši rezultat pa je dal kontekst velikosti 2.
lexical chain Hirst & St-Onge (1998) je rahlo izboljšal rezultate.


### Adapted Lesk \cite(bajeer2002adapted) \cite(ekedahl2004)
Adapted Lesk (od tu naprej AL) za vir pomenov besed uporablja slovar WordNet.
Algoritem izbere kontekst okoli tarčne besede. Nato za vsako besedo v kontekstu v slovarju poišče glos. Glos je sestavljen iz definicije definicije osnovnega pomena ter iz definicij pomenov nadpomenk. Najboljše rezultate je dal algoritem, ki je iskal pomene do druge nadpomenke v drevesu. 
Kontekst, ki je dal najboljše rezultate je vseboval 2 besedi levo in desno od tarčne besede. Te dve besedi sta morali biti vsebovani v WordNet, sicer jih je algoritem nadomestil. 

Potek algoritma je podoben kot pri OL.
Vsaka tarčna beseda ter beseda v kontekstu dobijo enega ali več pomenov iz WordNet. 
|bi| naj bo število pomenov besede bi. Algoritem primerja vse možne kombinacije pomenov besed in izbere tisto z najvišjo oceno. Če je število besed n, je število vseh kombinacij PI[n, i=0] |bi|. Če vzamemo, da je povprečno število pomenov besede m, lahko ocenimo časovno zahtevnost na O(m^n).

Eden od problemov, na katerega so raziskovalci naleteli je veliko število finih pomenov, ki jih je težko ločevati med sabo.


### Lesk z dodatnimi izboljšavami
V raziskavi, opisani v članku \cite(Ramakrishnan2004gloss) so preizkusili načine izgradnje in primerjave gosov.
Algoritem podobno kot AL iz slovarja Wordnet črpa opise pomenov. Tudi v tej raziskavi se je izkazalo, da najboljše rezultate dajejo združeni glosi same besede tre dveh nadpomenk.
Za primerjavo glosov so izdelali vektorje tf-igf in izračunali kosinusno podobnost (cosine similarity).  

Tf-igf je utež, podobna tf-idf. Izračunana za vsako besedo vseh glosov, ki jih primerjamo med sabo, po formuli: (pogostost besede) / (število glosov, v katerih se ta beseda pojavi). Intuitivno, tf-igf da večjo težo manj pogostim besedam.

Raziskava je pokazala, da najboljše rezultate daje Leskov algoritem, ki za kontekst uporablja 1 stavek, pri kateremu uporabi osnovne besede, brez njihovih glosov v WordNet. Glose za tarčno besedo izdela iz definicije te besede ter iz definicij prvih dveh nadpomenk.






SLOVAR:
glos: skupek besed, ki se uporablja za enolično definicijo pomena slovarske besede TODO



