﻿Ok, parsanje sskj2 bo malo težje, kot sem pričakoval.

Kaj naredim, ko naletim za 2 iztočnici?? Mislim beseda žvenketati je v slovarju 2x. Ali sta to dva pomena, ali je to kaj drugega?

Derp, očitno gre za 2 besedni obliki... žvenk, žvenk. Fak, to bo še zabavno.

Kaj, ko vržem vse skupaj v xmltodict in vržem stvar v mongodb, potem se bom pa ukvarjal z dobivanjem informacij.

Damn, izt_clean se ni dobro ujel z bazo.
Iščem “glasiti”, zapis v bazi je v vrstici 18288: “glas\preglas{i}ti”.
Izkaže se, da je beseda “glasiti se”. Shranjena v slovarju. Jaz dobim iz korpusa le “glasiti”. Kako rešiti zadevo?

Ok, iztočnicam, ki se končajo na “ se” sem odstranil “ se”. Upam, da bo delovalo.

Sskj2 nima idjev, kot jih ima SloWNet. To zna privesti do težav pri identifikaciji podobnih vnosov. Za zdaj je ID zgrajen iz popravljene iztočnice. Lahko bi dodali besedno vrsto, a vsi vnosi je nimajo...
Morda hash(hw + (besedna vrsta ali none)). Morda dodati še kakšen člen. Idk...

Parsanje te zmede... argh...

Ključna beseda ima lahko več vnosov v sskj2. Preveri to, nato popravi lesk, da bodo IDji iz sskja.

## Nedelja, 27.5.18
Sredi dneva fucking vaje z bendom. Nazaj bom prišel bolj skurjen kot bom šel tja. Poskusimo nekaj zastaviti pred vajami. Sskj2 imam prebran, glosse vlečem ven.  

* Zamenjaj sskj2 s sskj v algoritmih lesk_nltk in lesk_sl. 
* Backup collections. 
* Poženi algoritma.

Po tem imaš 2 izbiri: preglodaj članke za k-means ali pa začni graditi vue.js app. Mogoče tale: zrihtaj sskj, najdi članke, jih natistni in jih nesi na vajo, da lahko šprintaš skozi članek vsake toliko časa. 
Med vajo naj tečeta algoritma + bittorrent.

Gremo na delo :)


## Ponedeljek, 28.5.18
Začel s članki. 11:00 - 13:00. Zgraditi moram k-means based algoritem. 
Všeč mi je ideja, da za vsak hw zgradim feature vector, ki je len(vsi konteksti) in za vsak kontekst pove, kakšno vlogo ima hw v njem. 
Tu seveda pride v poštev sparse matrix.

Relevantni članki:

* [dependency triples](http://delivery.acm.org/10.1145/990000/980696/p768-lin.pdf?ip=82.192.58.162&id=980696&acc=OPEN&key=4D4702B0C3E38B35%2E4D4702B0C3E38B35%2E4D4702B0C3E38B35%2E6D218144511F3437&__acm__=1527504929_3587daf6215bcecd8036faee7aa5c4fc)(p768-lin.pdf)
Grajenje razmerij (hw, funktor, beseda). To stvar shranimo v db za hitre poizvedbe.  
Induction of Semantic Classes from Natural Language Text tudi uporablja to bazo in delno opisuje konstrukcijo.
* [Knjiga Manning 1999](http://ics.upjs.sk/~pero/web/documents/pillar/Manning_Schuetze_StatisticalNLP.pdf) (Manning_Schuetze...pdf) 
Tu notri je enačba za izračun bližine med hw in kontekstom. Iz teh ocen sestavi vektor.
* Naslednji korak je clustering teh vektorjev. Pustimo to za jutri.


14:30 - 16:10
Nadaljujem s članki. Prvi dve točki zgoraj.  
Najprej moram zgraditi bazo dep_triples.  

Bah, že cel dan gledam članke, pa nikamor ne gre... Sedaj imam narejen načrt, kako sestaviti feature vectorje. 

Ne vem... teli algoritmi končajo v nekih čudnih implementacijah... skoraj ne upam preizkušat, ker ne vem, kako bom lahko zagoravjal nekaj kar komaj razumem. V glavnem, rajši bom uporabil bol intuitivnega. Našel sem članek [arabic stuff](http://sci-hub.tw/10.1145/2812809), kjer uporabijo bisection k-means za razločevanje DOKUMENTOV. Jaz lahko stavek obravnavam kot dukument in jih ločujem med sabo. 

Vzemi hw: igrati. 
Poišči število “sensov” v sskj. - Lahko poskusiš elbow metodo. 
Vektor naj vsebuje: vektoriziran kup besed, ki so chain(w). 
Chain (w) naj vrne vse nadpomenke vse do korena v WordNet. 

Problemi: 

* implementiraj bisecting k-means,
* elbow method za prekinitev algoritma?

To bi moralo zadostovati; potem se loti vu.js applikacije; ob tem pa piši diplomo. 50/50 mentalne obremenitve. Glodanje skozi tele članke je kar mučno.


## Torek
Ok, včeraj nisem naredil ničesar. Danes pa v akcijo. Treba je ustvariti nujo od znotraj, kot pravi tisti filozof z brki. Mislim, da je nujno, da danes sprogramiram in evaluiram k-means algoritem. Imam še ogromno dela. Časa je malo.  

Najprej članek *abuaiadah2016*.  
Raziskovalec poskuša grupirati dokumente po pomenu. 
V članku primerja k-means in bkm. Uporabi različne meritve podobnosti (cosine, Persnos, Jaccard, ...).  
*Steinbach 2000* vsebuje postopek clusteringa, ki mu sledi avtor. Poglej, če je to knjiga iz literature. 

Implementiral sem k-means z bisekcijo in cosine similariy. Morda ne bo deloval dobro zaradi majhnega števila vhodnih primerov (če prva razpolovitev razpolovi napačno, je odločotev dokončno določena).


## 4.6.2018
Ok, gremo pisat. Uvod v razločevanje pomenov lahko najdem v knjigi “Jurafsky?”, k-means lahko opišem iz dveh člankov (arab + bisect km).

Let’s go.

Za uvod bi bilo fino citirati kakšen članek, ali pa vsaj sskj. No ja, za enkrat pustimo.

Gremo na k-means.

Ok, napisal cca 2.5 strani v celem dnevu. Sigh. Slabo. No ja, še enkrat preberi vse skupaj in pošlji profesorju.

Naredi še evaluacijo. Veliko dela imaš, pohiti. 
Zvečer opiši vse ocene evaluacije (par enačb) ter nariši tabelo.

## 5.6.2018
Torek. Gremo na delo, rad bi zaključil machine learning del in poslal napisano profesorju. Manjka le še eval.

Ok, začnimo s purity (manning et al 2008). 

Porabil kar nekaj časa, da sem popravil labeling v latex. Gremo implementirat purity. Formule v diplomi zgledajo kar lepo B).

Čez dan pisal, zdaj pa implementacija. Gremo!

## Sreda
Počasen si. Še vedno implementiraš evaluation. Ocene delujejo, samo še poženi po primerih.

Vprašaj profesorja, če je pravilno: algoritmi za WSD. Mogoče RPB?

Spremenil sem k pri k-means na 1 do 10. Poženimo še enkrat in ocenimo. 

Profesorju:
Pozdravljeni
Napisal sem del diplome, ki se ukvarja s strojnim učenjem. Za gospo Gantar moram urediti še nekatere dele spletne aplikacije, tako da se zdaj lotevam tega.

Prosim, če pogledate poglavja Razdvoumljanje pomenov besed in Evaluacija WSD.
Za razdvoumljanje pomenov besed uporabljam angleško kratico (WSD), ali bi bilo boljše uporablati slovensko različico (RPB)?

OK, machine learning je upam da opravljen. Gremo graditi aplikacijo. Login, routing - nauči se te dve stvari v Vue.js.

## 9.6.2018
Ok, tole bo slo. Narisal sem wireframe spletne aplikacije. Zdaj pa na polno: zgradi look and feel z bootstrapom. API ostane za konec. 

Malce me skrbi instalacija frontendovskih knjiznic. Mby prenesi module ter pozeni kar imas pri sebi?

Kako pozenem vue preko flask backenda?? Damn, malo vec dela, kot sem mislil. Treba bo spet pogledat CORS.

Ugh, CORS.
Ko bo slo v deployment, poskrbi da ni cross site scripting odprt za ves svet.

## 10.6.2018
Run, Forrest. Type that app!
Ok, dodal sem levi meni. Next stop, api: send frames + draw frames.
Good luck, soldier.


