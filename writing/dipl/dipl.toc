\select@language {english}
\select@language {slovene}
\contentsline {chapter}{Povzetek}{}{chapter*.2}
\select@language {english}
\contentsline {chapter}{Abstract}{}{chapter*.3}
\select@language {slovene}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Vezljivost}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Udele\IeC {\v z}enske vloge}{6}{section.2.1}
\contentsline {chapter}{\numberline {3}Obstoje\IeC {\v c}e re\IeC {\v s}itve}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}PDT-Vallex}{9}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Struktura PDT-Vallexa}{10}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Valen\IeC {\v c}ni okvir v PDT-Vallexu}{10}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}CROVALLEX}{11}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Struktura vnosa v CROVALLEX}{11}{subsection.3.2.1}
\contentsline {chapter}{\numberline {4}Opis re\IeC {\v s}itve}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}U\IeC {\v c}ni korpus}{16}{section.4.1}
\contentsline {section}{\numberline {4.2}Spletna aplikacija}{16}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Uporabni\IeC {\v s}ka izku\IeC {\v s}nja}{17}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Arhitektura spletne aplikacije}{19}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Razdvoumljanje pomenov besed}{20}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Leskov algoritem}{22}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Algoritem k-voditeljev}{27}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Druge metode razdvoumljanja}{29}{subsection.4.3.3}
\contentsline {chapter}{\numberline {5}Evalvacija razdvoumljanja}{33}{chapter.5}
\contentsline {chapter}{\numberline {6}Zaklju\IeC {\v c}ek}{37}{chapter.6}
\contentsline {chapter}{Literatura}{39}{chapter.6}
