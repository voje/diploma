\select@language {english}
\select@language {slovene}
\contentsline {chapter}{Povzetek}{}{chapter*.2}
\select@language {english}
\contentsline {chapter}{Abstract}{}{chapter*.3}
\select@language {slovene}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Vezljivost glagolov}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}Obsoje\IeC {\v c}e re\IeC {\v s}itve}{5}{chapter.3}
\contentsline {chapter}{\numberline {4}Opis re\IeC {\v s}itve}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Uporabljeni korpus}{7}{section.4.1}
\contentsline {section}{\numberline {4.2}Pregled nad korpusom}{7}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Spletna aplikacija}{7}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Arhitektura}{7}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Razdvoumljanje povenov povedi}{7}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Lesk}{7}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}KMeans}{8}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}Evaluacija}{9}{chapter.5}
\contentsline {chapter}{\numberline {6}Zaklju\IeC {\v c}ek}{11}{chapter.6}
\contentsline {chapter}{Literatura}{13}{chapter.6}
