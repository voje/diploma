\select@language {english}
\select@language {slovene}
\contentsline {chapter}{\numberline {1}Teorija vezljivosti}{}{chapter.1}
\contentsline {section}{\numberline {1.1}Vezljivost ali valenca}{}{section.1.1}
\contentsline {section}{\numberline {1.2}Udele\IeC {\v z}enske vloge}{}{section.1.2}
\contentsline {section}{\numberline {1.3}Vezljivost in NLP}{}{section.1.3}
\contentsline {section}{\numberline {1.4}FGD}{}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Umestitev FGD med lingvisti\IeC {\v c}ne teorije}{}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Zna\IeC {\v c}ilnosti FGD}{}{subsection.1.4.2}
\contentsline {section}{\numberline {1.5}Valen\IeC {\v c}ni okvir v FGD}{}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Notranji udele\IeC {\v z}enci in prosti modifikatorji}{}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Obveznost ali neobveznost udele\IeC {\v z}encev}{}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Dodeljevanje vlog v valen\IeC {\v c}nem okvirju}{}{subsection.1.5.3}
\contentsline {chapter}{\numberline {2}Valen\IeC {\v c}ni leksikoni}{}{chapter.2}
\contentsline {section}{\numberline {2.1}PDT-Vallex}{}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Valen\IeC {\v c}ni okvir v PDT-Vallex}{}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Izgradnja PDT-Vallex}{}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}CROVALLEX}{}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Struktura CROVALLEX}{}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}CROVALLEX: povr\IeC {\v s}inske strukture}{}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}CROVALLEX: globoke strukture ali funktorji}{}{subsection.2.2.3}
\contentsline {chapter}{\numberline {3}Slovenski korpusi}{}{chapter.3}
\contentsline {section}{\numberline {3.1}Korpusa FIDA in FidaPLUS}{}{section.3.1}
\contentsline {section}{\numberline {3.2}Jezikoslovno ozna\IeC {\v c}evanje slovenskega jezika - JOS}{}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Korpusa jos100k in jos1M}{}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Oblikoskladenjske specifikacije JOS}{}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Sporazumevanje v Slovenskem Jeziku - SSJ}{}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Korpus Gigafida}{}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Korpus Kres}{}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Zbirka GOS}{}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Korpus \IeC {\v S}olar}{}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Korpus Lektor}{}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}ccGigafida in ccKres}{}{subsection.3.3.6}
\contentsline {section}{\numberline {3.4}korpus ssj200k}{}{section.3.4}
\contentsline {chapter}{Literatura}{}{section.3.4}
